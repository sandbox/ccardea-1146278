<?php

/**
 * @file
 *
 * Include file for autoloaded classes.
 */
class gnode {
  //Property definitions
  public $gnid;
  public $gnode_type;
  public $geometry_type;
  public $name;
  public $wkb;
  public $maxx;
  public $maxy;
  public $minx;
  public $miny;
  public $uid; // Id of the user who made the last update.
  public $updated;
  public $oid; // Uid of the original creator.
  public $oname; // User name of original creator.
  public $created;
  public $published;

  /**
   * Create a new gnode
   *
   * @param $wkb
   *   A gnode_geometry object. Can be any class derived from gnode_geometry
   */
  public function __construct($wkb = NULL) {
    if ($wkb) {
      $this->wkb_set($wkb);
      $this->gnode_type = 'generic';
    }
  }

  /**
   * Sets the values to be used for indexing the database. How the values are
   * determined varies with the type of geometry. For some geometry types these
   * values are also used to draw the map.
   */
  private function index($wkb) {
    $bounds = $wkb->bounds();
    $this->maxx = $bounds['maxx'];
    $this->maxy = $bounds['maxy'];
    $this->minx = $bounds['minx'];
    $this->miny = $bounds['miny'];
  }

  /**
   * @param wkb
   *   A gnode geometry object
   */
  public function wkb_set($wkb) {
    $this->index($wkb);
    $this->geometry_type = $wkb->geometry;
    $this->wkb = serialize($wkb);
  }
  public function wkb_get() {
    return unserialize($this->wkb);
  }
}

class gnode_bundle {
  // Property definitions
  public $is_new;
  public $gnode_type; // The machine readable bundle name.
  public $label; // The human readable bundle name.
  public $description;
  public $options; // Geometry type, edit options, display options.


  public function __construct() {
    $this->is_new = FALSE;
  }

  public function options_set($options) {
    $this->options = serialize($options);
  }

  public function options_get() {
    return unserialize($this->options);
  }
}

class gnode_element {
  // Property definitions.
  public $eid; // Element id.
  public $label;
  public $gtype; // One of the gnode geometry constants.
  public $etype; // Element type - one of the gnode element-type constants.
}
