<?php

/**
 * @file
 * Contains geometry class implementations.
 */

/**
 * Abstract base class for Gnode geometry types
 *
 * Defines properties and methods common to all geometry classes
 */
abstract class gnode_geometry {

  /**
   * The geometry type for this class - one of the Gnode Geometry Type constants.
   */
  public $geometry;

  /**
   * PHP magic function __sleep() - tells PHP how to serialize the object
   */
  abstract public function __sleep();

  /**
   * Returns an array containing the minimum and maximum latitudes and longitudes
   * needed to construct a rectangle that contains the geometry object. Used
   * for indexing the database and displaying objects on a map.
   */
  abstract public function bounds();

}

  /**
   * Class for all point-type geometries, i.e., addresses, map features, etc
   */
class gnode_point extends gnode_geometry {

  /**
   * The point coordinates. $pointx is longitude, $pointy is latitude
  */
  public $pointx;
  public $pointy;

  /**
   * Sets up the point coordinates and geometry type.
   *
   * @param $x
   *   longitude
   * @param $y
   *   latitude
   */
  public function __construct($x, $y) {
    $this->pointx = $x;
    $this->pointy = $y;
    $this->geometry = GNODE_POINT;
  }

  public function __sleep() {
    $keys = get_object_vars($this);
    return array_keys($keys);
  }

  public function bounds() {
    return array(
      'maxx' => $this->pointx,
      'maxy' => $this->pointy,
      'minx' => $this->pointx,
      'miny' => $this->pointy,
   );
  }

  public function lat() {
    return $this->pointy;
  }

  public function lng() {
    return $this->pointx;
  }
}

/**
 * Geometry class for multipoint objects
 *
 * Used for displaying multiple points on a map.
 */
class gnode_multipoint extends gnode_geometry {

  /**
   * An array of gnode_point objects
   */
  public $points;

  public $bounds;
  public $geometry;

  /**
   * Sets up the points array and computes the bounds.
   *
   * @param $gnodes
   *  An array of gnode objects
   */
  public function __construct($gnodes) {
    $this->geometry = GNODE_MULTIPOINT;
    $this->points_set($gnodes);
    $this->bounds_set($gnodes);
  }

  public function __sleep() {
    $keys = get_object_vars($this);
    return array_keys($keys);
  }

  public function bounds() {
    return isset($this->bounds) ? $this->bounds : FALSE;
  }

  private function points_set($gnodes) {
    $points = array();
    if (!empty($gnodes)) {
      foreach($gnodes as $gnode) {
        $points[] = unserialize($gnode->wkb);
      }
    }
    $this->points = $points;
  }

  private function bounds_set($gnodes) {
  if (empty($gnodes)) {
    $bounds = array(
      'maxx' => 0,
      'maxy' => 0,
      'minx' => 0,
      'miny' => 0,
    );
    $this->bounds = $bounds;
    return;
  }
  $maxx;
  $maxy;
  $minx;
  $miny;
  foreach ($gnodes as $row) {
    if (!isset($maxx)) {
      $maxx = $row->maxx;
      $maxy = $row->maxy;
      $minx = $row->minx;
      $miny = $row->miny;
      continue;
    }
    $maxx = $row->maxx > $maxx ? $row->maxx : $maxx;
    $maxy = $row->maxy > $maxy ? $row->maxy : $maxy;
    $minx = $row->minx < $minx ? $row->minx : $minx;
    $miny = $row->miny < $miny ? $row->miny : $miny;
  }
  $bounds = array(
    'maxx' => $maxx,
    'maxy' => $maxy,
    'minx' => $minx,
    'miny' => $miny,
  );
  $this->bounds = $bounds;
  }
}
