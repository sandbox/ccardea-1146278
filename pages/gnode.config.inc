<?php

/**
 * @file admin.inc
 * Contains admin pages and related code
 */

/**
 * Menu callback - renders a page with menu item links
 */
function gnode_admin_menu_block_page() {
  $item = menu_get_item();
  if ($content = system_admin_menu_block($item)) {
    $output = theme('admin_block_content', array('content' => $content));
  }
  else {
    $output = t('You do not have any administrative items.');
  }
  return $output;
}

/**
 * The configuration form for the gnode module
 */
function gnode_config_form($form, &$form_state) {
  $form['gnode_default_unit'] = array(
    '#type' => 'radios',
    '#title' => t('Default unit of measurement'),
    '#options' => array(
      'Miles' => 'Miles',
      'Kilometers' => 'Kilometers'
    ),
    '#default_value' => variable_get('gnode_default_unit', 'Kilometers'),
  );

  return system_settings_form($form);
}

