<?php

/**
 * @file gnode.admin.inc
 *  Forms for administering gnodes
 *
 * This code is adapted from the node module, for use by gnodes.
 * The code is in rudimentary form and will be further developed.
 */

/**
 * TODO 
 * A possible addition is a link to the gnode author, but this requires
 * having the username available.
 *
 * Must have a gnode type filter.
 * Remove the delete column if delete is not authorized? Administer permission 
 * is required to view the page, so I'm not sure this is necessary.
 * Would be nice to have a proximity filter on the page.
 */

/*
 * Builds the main gnode admin page.
 */
function gnode_overview_form($form, &$form_state) {
  $header = array(
    'name' => array('data' => t('Name'), 'field' => 'g.name'),
    'type' => array('data' => t('Type'), 'field' => 'g.gnode_type'),
    'updated' => array('data' => t('Updated'), 'field' => 'g.updated', 'sort' => 'desc'),
    'operations' => array('data' => t('Operations'), 'colspan' => 2),
  );

  $options = array();

  $query = db_select('gnode', 'g')->extend('PagerDefault')->extend('TableSort');
  $ids = $query
    ->fields('g', array('gnid'))
    ->limit(30)
    ->orderByHeader($header)
    ->execute()
    ->fetchCol();

  if (!empty($ids)) {
    $gnodes = gnode_load_multiple($ids);
    $destination = drupal_get_destination();
    foreach ($gnodes as $gnode) {
      $options[$gnode->gnid] = array(
        'name' => array(
          'data' => array(
            '#type' => 'link',
            '#title' => t('@name', array('@name' => $gnode->name)),
            '#href' => 'gnode/' . $gnode->gnid,
          ),
        ),
        'type' => check_plain($gnode->gnode_type),
        'updated' => format_date($gnode->updated, 'short'),
        'edit' => array(
          'data' => array(
            '#type' => 'link',
            '#title' => t('edit'),
            '#href' => 'gnode/' . $gnode->gnid . '/edit',
            '#options' => array('query' => $destination),
          ),
        ),
        'delete' => array(
          'data' => array(
            '#type' => 'link',
            '#title' => t('delete'),
            '#href' => 'gnode/' . $gnode->gnid . '/delete',
            '#options' => array('query' => $destination),
          ),
        ),
      );
    }
  }
  $form['gnodes'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $options,
    '#empty' => t('No Gnodes available.'),
  );
  $form['pager'] = array('#markup' => theme('pager'));

  return $form;
}
