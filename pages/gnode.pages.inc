<?php

/**
 * @file point.inc author: Christopher Cardea, 2011
 * Contains code for gnode add and gnode view pages
 *
 * These pages have been designed for viewing gnodes of type GNODE_POINT.
 * It may be possible to use these pages for other geometry types, in which case
 * the directory structure may be reconsidered.
 */

/**
 * View a gnode page
 *
 * This page will handle all geometry types. 
 */
function gnode_page_view($gnode) {
  drupal_set_title($gnode->name);
  $options = gnode_get_bundle_options($gnode->gnode_type);
  $build = array();
  if ($options['show_map']) {
    $wkb = unserialize($gnode->wkb);
    
    //Create the render array
    $build['map'] = element_info($options['map']);
    $build['map']['#geometry'] = $wkb;

  }
  // Call hook_entity_view_alter().
  foreach (module_implements('entity_view_alter') as $module) {
    $function = $module . '_entity_view_alter';
    $function($build, 'gnode');
  }
  return $build;
}

/**
 * Build gnode add/edit form
 *
 * This page will only handle bundles of type point, because it takes as input
 * latitude and longitude fields. This would have to be changed or different 
 * forms will have to be used for other geometry types.
 *
 * TODO use $_REQUEST OR $_SESSION instead of variable_set, to avoid clearing the cache
 */
function gnode_add_form($form, &$form_state, $bundle, $gnode = NULL) {
  $options = unserialize($bundle->options);

  $form['name'] = array(
    '#title' => t('Name'),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#weight' => -5,
  );

  $form['input'] = element_info($options['edit_options']);

  variable_set('gnode_show_map', $options['show_map']);
  if ($options['show_map']) {
    $form['map'] = element_info($options['map']);
  } 

  variable_set('gnode_show_geocoder_results', $options['other_options']['show_results']);

  // The result div is always available to show Geocoder errors
  $form['result'] = array(
    '#type' => 'markup',
    '#markup' => '<div id="result"></div>',
  );
  $form['type'] = array(
    '#type' => 'value',
    '#value' => $bundle->gnode_type,
  );
  $form['latitude'] = array(
    '#title' => t('Latitude'),
    '#attributes' => array('id' => 'gnode-lat'),
    '#type' => 'hidden',
  );
  $form['longitude'] = array(
    '#title' => t('Longitude'),
    '#attributes' => array('id' => 'gnode-lng'),
    '#type' => 'hidden',
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  // If $gnode is set, the form is being edited -  set default values
  if (isset($gnode)) {
    $form['#gnid'] = $gnode->gnid;
    $form['name']['#default_value'] = $gnode->name;
    $wkb = unserialize($gnode->wkb);
    $form['map']['#geometry'] = $wkb;
    $form['latitude']['#default_value'] = $wkb->pointy;
    $form['longitude']['#default_value'] = $wkb->pointx;
  }

  return $form;
}
/**
 * Validator for gnode add form
 */
function gnode_add_form_validate($form, &$form_state) {
  if (empty($form_state['values']['latitude']) || empty ($form_state['values']['longitude'])) {
    form_set_error('op', t('Invalid geocoder result'));
  }
}

/**
 * Submit handler for gnode add form
 */
function gnode_add_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  $wkb = new gnode_point($values['longitude'], $values['latitude']);

  $gnode = new gnode($wkb);
  $gnode->gnode_type = $values['type'];
  $gnode->name = $values['name'];

  // If this is an existing gnode, set the gnid 
  if (isset($form['#gnid'])) {
    $gnode->gnid = $form['#gnid'];
  }

  gnode_save($gnode);
  $form_state['redirect'] = 'gnode/' . $gnode->gnid;
  return $gnode;
}

/**
 * Menu callback - edit a gnode.
 *
 * @param $gnode
 *  A gnode object
 *
 * @return
 *  Returns the gnode add/edit form 
 */
function gnode_page_edit($gnode) {
  drupal_set_title(t('<em>Edit</em> @title', array('@title' => $gnode->name)), PASS_THROUGH);
  $bundle = gnode_bundle_load($gnode->gnode_type);
  return drupal_get_form('gnode_add_form', $bundle, $gnode);
}

/**
 * Confirm a gnode delete request
 *
 * @return
 *  Returns a Drupal confim form
 */
function gnode_delete_confirm($form, &$form_state, $gnode) {
   $form['#gnode'] = $gnode;
  // Always provide entity id in the same form key as in the entity edit form.
  $form['gnid'] = array(
    '#type' => 'value',
    '#value' => $gnode->gnid,
  );

  $question = t('<em>Are you sure you want to delete @name?</em>', array('@name' => $gnode->name));
  $path = 'gnode/' . $gnode->gnid;
  $description = t('This action cannot be undone.');
  $yes = t('Delete');
  $no = t('Cancel');
  $name = 'confirm';

  return confirm_form($form, $question, $path, $description, $yes, $no, $name);

}

/**
 * Submit handler for the gnode delete confirm form
 */
function gnode_delete_confirm_submit($form, $form_state) {
  gnode_delete($form_state['values']['gnid']);
  $gnode = $form['#gnode'];
  drupal_set_message(t('Gnode: <em>@name</em> has been deleted.', array('@name' => $gnode->name)));
  //TODO watchdog message?
  drupal_goto();
}

