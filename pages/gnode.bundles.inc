<?php

/**
 * @file bundles.inc
 *
 * Contains pages and forms for administering gnode bundles
 */

/**
 * Displays the gnode bundles admin overview page
 */
function gnode_overview_bundles() {
  $bundles = gnode_get_bundles();
  $header = array(
    t('Name'),
    array(
      'data' => 'Operations',
      'colspan' => 2,
    ),
  );
  $rows = array();
  foreach ($bundles as $key) {
    $url = str_replace('_', '-', $key->gnode_type);
    $row = array();
    $row[] = array(
      'data' => array(
        '#theme' => 'gnode_bundle_admin_row',
        '#label' => $key->label,
        '#description' => $key->description,
      ),
    );
    $row[] = l('edit', 'admin/gnodes/bundles/edit/' . $url);
    $row[] = l('delete', 'admin/gnodes/bundles/delete/' . $url);
    $rows[] = $row;
  }
  $build['gnode_table'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#empty' => t('No Gnode bundles available.'),
  );
  return $build;
}

/**
 * Add/edit a gnode bundle
 */
function gnode_bundle_form($form, &$form_state, $bundle = NULL) {
  $options = array();
  if ($bundle) {
    $options = unserialize($bundle->options);
  }
  $form['is_new'] = array(
    '#type' => 'hidden',
    '#value' => $bundle ? FALSE : TRUE,
  );
  $form['bundle'] = array(
    '#type' => 'fieldset',
  );
  $form['bundle']['type'] = array(
    '#type' => 'machine_name',
    '#title' => t('Bundle Name'),
    '#description' => t('The machine-readable name of the gnode bundle. This must contain only lower-case letters, numbers, and underscores.'),
    '#maxlength' => 32,
    '#size' => 32,
    '#machine_name' => array(
      'exists' => 'gnode_bundle_load',
    ),
    '#default_value' => $bundle ? $bundle->gnode_type : '',
  );
  $form['bundle']['label'] = array(
    '#type' => 'textfield',
    '#title' => t('Bundle Label'),
    '#description' => t('The human-readable label for this bundle.'),
    '#maxlength' => 32,
    '#size' => 32,
    '#required' => TRUE,
    '#default_value' => $bundle ? $bundle->label : '',
  );
  $form['bundle']['description'] = array(
    '#type' => 'textarea',
    '#title' => 'Description',
    '#default_value' => $bundle ? $bundle->description : '',
  );

  $form['settings'] = array(
    '#type' => 'vertical_tabs',
    '#prefix' => '<div id="gnode-ajax-wrapper">',
    '#suffix' => '</div>',
  );
  $form['geometry'] = array(
    '#type' => 'fieldset',
    '#title' => t('Geometry Options'),
    '#description' => t('Input and display options depend on the geometry type.'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#group' => 'settings',
  );
  $form['geometry']['geometry'] = array(
    '#type' => 'radios',
    '#title' => t('Geometry Type'),
    '#default_value' => $bundle ? $options['geometry'] : GNODE_POINT,
    '#options' => gnode_geometries(),
    '#ajax' => array(
      'callback' => 'gnode_options',
      'wrapper' => 'gnode-ajax-wrapper',
      'method' => 'replace',
      'effect' => 'fade',
    ),
  );
  $form['edit'] = array(
    '#type' => 'fieldset',
    '#title' => t('Input Options'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#group' => 'settings',
  );

  $form['edit']['edit_options'] = array(
    '#type' => 'radios',
    '#title' => t('Input Options'),
    '#options' => gnode_bundle_options($form, $form_state, GNODE_INPUT),
    '#default_value' => $bundle ? $options['edit_options'] : NULL,
  );

  $form['display'] = array(
    '#type' => 'fieldset',
    '#title' => t('Display Options'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#group' => 'settings',
  );
  $form['display']['show_map'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display map'),
    '#return_value' => TRUE,
    '#default_value' => $bundle ? $options['show_map'] : FALSE,
  );
  $form['display']['map'] = array(
    '#type' => 'radios',
    '#title' => t('Map Options'),
    '#options' => gnode_bundle_options($form, $form_state, GNODE_DISPLAY),
    '#default_value' => $bundle ? $options['map'] : NULL,
  );
  $form['display']['other_options'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Other Options'),
    '#options' => array(
      'show_results' => t('Show geocoder results (input form only)'), //This should probably be a provider option
    ),
    '#default_value' => $bundle ? $options['other_options'] : array(),
  );

  $form['actions'] = array(
    '#type' => 'actions',
  );
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save bundle'),
    '#weight' => 40,
  );

 return $form;
}

/**
 * Get the available geometry types
 *
 * This can vary depending on what provider modules are installed
 *
 * @return
 *  Returns an associative array keyed by gnode geometry type constants
 */
function gnode_geometries() {

  $geo = gnode_get_geometry();
  if (!$geo) {
    drupal_set_message(t('There are no Gnode providers installed. You must install a Gnode provider in order to create a Gnode bundle.'), 'error');
  }

  $options = array();
  foreach ($geo as $type) {
    switch ($type) {
      case GNODE_POINT:
        $options[GNODE_POINT] = t('Point');
        break;
      case GNODE_LINESTRING:
        $options[GNODE_LINESTRING] = t('Line');
        break;
      case GNODE_POLYGON:
        $options[GNODE_POLYGON] = t('Polygon');
        break;
    }
  }
  return $options;
}

/**
 * AJAX callback for the $form['geometry'] element
 *
 * @return
 *   Returns the updated settings section containing available options based on
 *   the selected geometry type.
 */
function gnode_options($form, &$form_state) {
  return $form['settings'];
}

/**
 * Create a list of edit options based on bundle geometry-type & element-type
 *
 * @param $etype
 *  Element type - one of the gnode element type constants
 * @return
 *    Returns an associative array of input options, keyed by the element id.
 */
function gnode_bundle_options(&$form, &$form_state, $etype) {
  $options = array();
  $result = array();
  if (array_key_exists('values', $form_state) && array_key_exists('geometry', $form_state['values']) && !empty($form_state['values']['geometry'])) {
    $result = gnode_get_elements_by_geometry($form_state['values']['geometry']);
  }
  else {
    $result = gnode_get_elements_by_geometry($form['geometry']['geometry']['#default_value']);
  }
  foreach ($result as $row) {
    if ($row['etype'] == $etype) {
      $options[$row['eid']] = t('@value', array('@value' => $row['label']));
    }
  }
  return $options;
}

/**
 * Validation handler for gnode_bundle_form
 */
function gnode_bundle_form_validate($form, &$form_state) {
  // Remove these validation checks if default values are supplied
  if (empty($form_state['values']['edit_options'])) {
    $msg = t('Please choose an input option');
    form_set_error('edit', $msg);
  }

  if ($form_state['values']['show_map'] && empty($form_state['values']['map'])) {
    $msg = t("'Display Map' is checked. Please choose a map to display or uncheck 'Display Map'.");
    form_set_error('display', $msg);
  }
}

/**
 * Submit handler for gnode_bundle_form
 */
function gnode_bundle_form_submit($form, &$form_state) {
  $options = array(
    'geometry' => $form_state['values']['geometry'],
    'edit_options' => $form_state['values']['edit_options'],
    'other_options' => $form_state['values']['other_options'],
    'show_map' => $form_state['values']['show_map'],
    'map' => $form_state['values']['map'],
  );
  $bundle = new gnode_bundle();
  $bundle->is_new = $form_state['values']['is_new'];
  $bundle->gnode_type = $form_state['values']['type'];
  $bundle->label = $form_state['values']['label'];
  $bundle->description = $form_state['values']['description'];
  $bundle->options_set($options);
  gnode_bundle_save($bundle);
  $form_state['redirect'] = 'admin/gnodes/bundles';
}

/**
 * Display the delete confirm form
 */
function gnode_bundle_delete_confirm($form, &$form_state, $bundle) {
  $form['machine_name'] = array('#type' => 'value', '#value' => $bundle->gnode_type);
  $form['label'] = array('#type' => 'value', '#value' => $bundle->label);

  $message = t('Are you sure you want to delete the gnode bundle %type?', array('%type' => $bundle->label));
  $caption = '';
  $num_gnodes = db_query("SELECT COUNT(*) FROM {gnode} WHERE gnode_type = :type", array(':type' => $bundle->gnode_type))->fetchField();
  if ($num_gnodes) {
    $caption .= '<p>' . format_plural($num_gnodes, '%type is used by 1 piece of content on your site. If you remove this content type, you will not be able to edit the %type content and it may not display correctly.', '%type is used by @count pieces of content on your site. If you remove %type, you will not be able to edit the %type content and it may not display correctly.', array('%type' => $bundle->label)) . '</p>';
  }

  $caption .= '<p>' . t('This action cannot be undone.') . '</p>';

  return confirm_form($form, $message, 'admin/gnodes/bundles', $caption, t('Delete'));
}

/**
 * Submit handler for bundle delete form
 */
function gnode_bundle_delete_confirm_submit($form, &$form_state) {
  gnode_bundle_delete($form_state['values']['machine_name']);

  $t_args = array('%name' => $form_state['values']['label']);
  drupal_set_message(t('The gnode bundle %name has been deleted.', $t_args));
  watchdog('gnode', 'Deleted gnode bundle %name.', $t_args, WATCHDOG_NOTICE);

  menu_rebuild();

  $form_state['redirect'] = 'admin/gnodes/bundles';
  return;
}
