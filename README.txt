Gnode

The Gnode module provides an entity that is designed for content that is
location aware or intended to be displayed on a map. It will work with any map
service that provides Gnode integration.

Installation:

There are no special installation requirements. Install as you would any Drupal
module. Download and unpack in the sites/all/modules directory, then enable from
the modules admin page.

Dependencies:

Map Providers

Gnode requires at least one additional module to do anything useful. You will
also need to download and install a map provider. Currently Gnode Google is the
only map provider. As other map providers are implemented, you may install and
use as many as you like. Map providers are designed to work together.

Views and Proximity Searching

Multipoint views and proximity searching are currently provided by separate
modules: Gnode Views and Gnode Proximity. You will need to download and install
these modules separately.

For a full explanation of features, demos, screenshots, and developer info,
please visit http://gnode.ccardea.com.
